const { app, BrowserWindow, globalShortcut, ipcMain, nativeTheme } = require("electron");
const path = require("path");

function createWindow () {
    const win = new BrowserWindow({
        width: 1200,
        height: 800,
        icon: __dirname + "/assets/app.png",
        webPreferences: {
            devTools: true,
            nodeIntegration: true,
            nodeIntegrationInWorker: true,
            preload: path.join(__dirname, "src", "scripts", "preload.js")
        }
    });

    win.loadFile(path.join(__dirname, "src", "index.html"));

    //win.webContents.openDevTools();

    ipcMain.handle('dark-mode:toggle', () => {
        if (nativeTheme.shouldUseDarkColors) {
            nativeTheme.themeSource = 'light'
        } else {
            nativeTheme.themeSource = 'dark'
        }
        return nativeTheme.shouldUseDarkColors
    })

    ipcMain.handle('dark-mode:system', () => {
        nativeTheme.themeSource = 'system'
    })
}

app.setAppUserModelId("Gis-Desktop Data App")

app.whenReady().then(() => {
    if (process.platform !== 'darwin') {
        globalShortcut.register('CommandOrControl+Q', () => {
            app.exit();
        });
    }

    createWindow();

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) {
            createWindow();
        }
    })
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
})