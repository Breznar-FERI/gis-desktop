const { _electron: electron, test, expect} = require('@playwright/test');

let window;
let electronApp;

test.beforeAll(async () => {
    electronApp = await electron.launch({ args: ['main.js'] });
    window = await electronApp.firstWindow();
});

test("Check window title.", async () => {
    const windowName = await window.title();
    await expect(windowName).toBe("Gis-Desktop Data App");
});

test("Click on Geojson tab", async () => {
    await window.click('text=Dodaj Geojson');
    await window.screenshot({path: 'playwright-screenshots/geojson-tab-screen.png'});
});

test("Click on WKT tab", async () => {
    await window.click('text=Dodaj WFS nastavitev');
    await window.screenshot({path: 'playwright-screenshots/wkt-tab-screen.png'});
});

test.afterAll(async () => {
    await window.context().close();
    await window.close();
});
