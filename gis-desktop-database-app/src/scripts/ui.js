const {addNewGeojson, addNewWfsSettings} = require("./geodata-adder");
const {localDatabase} = require("./local-database");
const axios = require("axios");
const {refreshFeatureData} = require("./feature-loader");
const {refreshWfsData} = require("./wfs-loader");


function bindAddForms() {
    let geojsonTab = document.querySelector("#geojsonTabHeader");
    geojsonTab.addEventListener("click", (e) => {
        document.querySelectorAll(".add-tab-btn")
            .forEach((elem) => {
                _removeActiveFromTabElement(elem)
            });
        document.querySelector("#addWfsSettingsForm").classList.add("hidden");
        document.querySelector("#addGeojsonForm").classList.remove("hidden");
        document.querySelector("#allWfsDataTable").classList.add("hidden");
        document.querySelector("#allDataTable").classList.remove("hidden");
        _setActiveForTabElement(geojsonTab);
    });

    let wfsTab = document.querySelector("#wfsTabHeader");
    wfsTab.addEventListener("click", (e) => {
        document.querySelectorAll(".add-tab-btn")
            .forEach((elem) => {
                _removeActiveFromTabElement(elem);
            });

        document.querySelector("#addGeojsonForm").classList.add("hidden");
        document.querySelector("#addWfsSettingsForm").classList.remove("hidden");
        document.querySelector("#allDataTable").classList.add("hidden");
        document.querySelector("#allWfsDataTable").classList.remove("hidden");
        _setActiveForTabElement(wfsTab);
    });
}

function bindAddButtons() {
    let geojsonButton = document.querySelector("#addGeojsonBtn");
    let wfsButton = document.querySelector("#addWfsBtn");

    geojsonButton.addEventListener("click", addNewGeojson);
    wfsButton.addEventListener("click", addNewWfsSettings);
}

function bindSyncDBButton() {
    document.querySelector("#syncDatabase").addEventListener("click", async (e) => {
        new Notification("Opozorilo", { body: `Začetek sinhronizacije lokalne baze z odaljeno` });

        let toDeleteDatabase = await localDatabase.to_delete.toArray();
        for (const toDelete of toDeleteDatabase) {
            if (toDelete.type == "wfs") {
                await axios.delete(`http://localhost:3000/api/v1/wms/delete/${toDelete.id}`);
            } else if (toDelete.type == "geojson") {
                await axios.delete(`http://localhost:3000/api/v1/geojson/delete/${toDelete.id}`);
            }
        }

        await axios
            .get("http://localhost:3000/api/v1/geojson/get-all")
            .then((response) => {
                let featureData = response.data || {};
                featureData.forEach(async (feature) => {
                    await localDatabase.features.put({
                        id: feature.id,
                        name: feature.name,
                        group: feature.group,
                        type: feature.type,
                        coordinates: feature.coordinates,
                        properties: feature.properties
                    });
                });
            });

        await axios
            .get("http://localhost:3000/api/v1/wms/get-all")
            .then((response) => {
                let featureData = response.data || {};
                featureData.forEach(async (feature) => {
                    await localDatabase.wfs.put({
                        id: feature._id,
                        name: feature.name,
                        group: feature.group,
                        url: feature.url,
                        properties: feature.properties
                    });
                });
            });

        refreshFeatureData();
        refreshWfsData();

        new Notification("Opozorilo", { body: `Končana sinhronizacija z odaljeno podatkovno bazo` });
    });
}

function _removeActiveFromTabElement(elem) {
    elem.classList.remove("active");
    elem.classList.remove("text-blue-600");
    elem.classList.remove("bg-gray-100");
    elem.classList.add("hover:bg-gray-50");
    elem.classList.add("hover:text-gray-600");
}

function _setActiveForTabElement(elem) {
    elem.classList.add("active");
    elem.classList.add("text-blue-600");
    elem.classList.add("bg-gray-100");
}

module.exports = { bindAddForms, bindAddButtons, bindSyncDBButton }