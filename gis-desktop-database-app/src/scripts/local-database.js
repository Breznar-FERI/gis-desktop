const Dexie = require('dexie');
const localDatabase = new Dexie("dexieDB");

localDatabase.version(1).stores({
    features: 'id,name,group,type,coordinates,properties',
    wfs: 'id,name,group,url,properties',
    to_delete: 'id,type',
});

localDatabase.open().catch(function(error){
    console.error(`There was an error connecting to local database: ${error}`);
});

module.exports = { localDatabase }