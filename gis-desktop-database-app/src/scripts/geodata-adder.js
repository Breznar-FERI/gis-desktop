const {refreshFeatureData} = require("./feature-loader");
const axios = require("axios");
const {refreshWfsData} = require("./wfs-loader");

function addNewGeojson() {
    let geojsonString = document.querySelector("#newGeojson").value

    if (geojsonString == "") {
        new Notification("Napaka", { body: "Polje za Geojson je prazno" });
        return;
    }

    let geojsonObj = JSON.parse(geojsonString);

    axios
        .post("http://localhost:3000/api/v1/geojson/save", geojsonObj)
        .then((response) => {
            refreshFeatureData();
            _clearGeojsonInputs();
            new Notification("Uspesno shranjeno", { body: "Geojson je bil uspesno shranjen v podatkovno bazo" });
        })
        .catch((err) => {
            new Notification({ title: "Napaka", body: err }).show();
        });
}

function addNewWfsSettings() {
    let wfsName = document.querySelector("#newWfsName").value;
    let wfsGroup = document.querySelector("#newWfsGroup").value;
    let wfsUrl = document.querySelector("#newWfsUrl").value;
    let wfsProperties = document.querySelector("#newWfs").value;

    if (wfsName == "" || wfsGroup == "" || wfsUrl == "" || wfsProperties == "") {
        new Notification("Napaka", { body: "Majnkajoci podatki za WFS" });
        return;
    }

    const wfsObj = {
        name: wfsName,
        group: wfsGroup,
        url: wfsUrl,
        properties: JSON.parse(wfsProperties)
    }

    axios
        .post("http://localhost:3000/api/v1/wms/save", wfsObj)
        .then((response) => {
            refreshWfsData();
            _clearWfsInputs();
            new Notification("Uspesno shranjeno", { body: "WFS je bil uspesno shranjen v podatkovno bazo" });
        })
        .catch((err) => {
            new Notification("Napaka", { body: err });
        });
}

function _clearGeojsonInputs() {
    document.querySelector("#newGeojson").value = "";
}

function _clearWfsInputs() {
    document.querySelector("#newWfsName").value = "";
    document.querySelector("#newWfsGroup").value = "";
    document.querySelector("#newWfsUrl").value = "";
    document.querySelector("#newWfs").value = "";
}

module.exports = { addNewGeojson, addNewWfsSettings }