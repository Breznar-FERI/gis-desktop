function bindSearch() {
    document.querySelector("#searchInput")
        .addEventListener("keyup", _startSearch);

    document.querySelector("#clearSearch").addEventListener("click", (e) => {
        document.querySelector("#searchInput").value = "";
        document.querySelectorAll(".geodata-row").forEach((elem) => elem.classList.remove("hidden"));
        document.querySelector("#clearSearch").parentElement.classList.add("hidden");
    });
}

function _startSearch() {
    let term = document.querySelector("#searchInput").value;

    document.querySelectorAll(".geodata-row").forEach((elem) => elem.classList.remove("hidden"));

    if (term == "") {
        document.querySelector("#clearSearch").parentElement.classList.add("hidden");
        return;
    }

    if (document.querySelector("#geojsonTabHeader").classList.contains("active")) {
        _searchInFeatureTable(term);
    } else if (document.querySelector("#wfsTabHeader").classList.contains("active")) {
        _searchInWfsTable(term);
    }

    document.querySelector("#clearSearch").parentElement.classList.remove("hidden");
}

function _searchInFeatureTable(term) {
    Array.from(document.querySelectorAll("#allDataTable tbody tr"))
        .filter((elem) => !_isTermIn(elem, term))
        .forEach((elem) => elem.classList.add("hidden"));
}

function _searchInWfsTable(term) {
    Array.from(document.querySelectorAll("#allWfsDataTable tbody tr"))
        .filter((elem) => !_isTermIn(elem, term))
        .forEach((elem) => elem.classList.add("hidden"));
}

function _isTermIn(elem, term) {
    let found = false;
    Array.from(elem.children).forEach((td) => {
       if (td.innerText.includes(term)) {
           found = true;
           return;
       }
    });
    return found;
}

module.exports = { bindSearch }