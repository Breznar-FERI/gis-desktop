const axios = require("axios");
const {localDatabase} = require("./local-database");

function loadAllFeatures() {
    axios
        .get("http://localhost:3000/api/v1/geojson/get-all")
        .then((response) => {
            let featureData = response.data || {};

            featureData.forEach(async (feature) => {
                await localDatabase.features.put({
                    id: feature.id,
                    name: feature.name,
                    group: feature.group,
                    type: feature.type,
                    coordinates: feature.coordinates,
                    properties: feature.properties
                });
            });

            refreshFeatureData();
        })
        .catch((err) => {
            new Notification("Napaka", { body: `Pri nalaganju podatkov iz streznika je prislo do napake: ${err}, uporabljeni bodo lokalno shranjeni podatki.` });
        });

    refreshFeatureData();
}

function refreshFeatureData() {
    document.querySelector("#allDataTable tbody").innerHTML = "";
    const allDataTableElement = document.querySelector("#allDataTable tbody");

    localDatabase.features
        .toArray()
        .then((arr) => {
            arr.forEach((feature) => {
                let newRow = document.createElement("tr");
                newRow.setAttribute("id", `featureRow-${feature.id}`);
                newRow.classList.add("geodata-row");
                let idElement = document.createElement("td");
                idElement.classList.add("text-center");
                idElement.innerText = feature.id;
                newRow.appendChild(idElement);

                let typeElement = document.createElement("td");
                typeElement.classList.add("text-center");
                typeElement.innerText = feature.type;
                newRow.appendChild(typeElement);

                let nameElement = document.createElement("td");
                nameElement.classList.add("text-center");
                nameElement.innerText = feature.name;
                newRow.appendChild(nameElement);

                let groupElement = document.createElement("td");
                groupElement.classList.add("text-center");
                groupElement.innerText = feature.group;
                newRow.appendChild(groupElement);

                let deleteElement = document.createElement("td");
                deleteElement.classList.add("text-center");
                let deleteButton = document.createElement("button");
                deleteButton.classList.add("bg-red-500");
                deleteButton.classList.add("hover:bg-blue-700");
                deleteButton.classList.add("text-white");
                deleteButton.classList.add("font-bold");
                deleteButton.classList.add("py-2");
                deleteButton.classList.add("px-4");
                deleteButton.classList.add("rounded-full");
                deleteButton.innerText = "Izbriši";
                deleteButton.addEventListener("click", (e) => {
                    _deleteFeature(feature.id)
                });
                deleteElement.appendChild(deleteButton);
                newRow.appendChild(deleteElement);

                allDataTableElement.appendChild(newRow);
            });
        });
}

async function _deleteFeature(id) {
    await localDatabase.to_delete.put({
        id: id,
        type: "geojson"
    });

    let element = document.querySelector(`#featureRow-${id}`);
    element.parentNode.removeChild(element);

    new Notification("Uspesno izbrisano", {body: "Geojson je bil uspesno izbrisani iz podatkovne baze"});

    axios
        .delete(`http://localhost:3000/api/v1/geojson/delete/${id}`)
        .then((response) => {
            localDatabase.to_delete
                .where({ id: id, type: "geojson" })
                .delete();
        });
}

module.exports = {loadAllFeatures, refreshFeatureData}