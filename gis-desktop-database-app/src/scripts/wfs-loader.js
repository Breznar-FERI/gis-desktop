const axios = require("axios");
const {localDatabase} = require("./local-database");

function loadAllWfs() {
    axios
        .get("http://localhost:3000/api/v1/wms/get-all")
        .then((response) => {
            let featureData = response.data || {};

            featureData.forEach(async (feature) => {
                await localDatabase.wfs.put({
                    id: feature._id,
                    name: feature.name,
                    group: feature.group,
                    url: feature.url,
                    properties: feature.properties
                });
            });

            refreshWfsData();
        })
        .catch((err) => {
            new Notification("Napaka", {  body: `Pri nalaganju podatkov je prislo do napake: ${err}` });
        });

    refreshWfsData();
}

function refreshWfsData() {
    document.querySelector("#allWfsDataTable tbody").innerHTML = "";
    const allDataTableElement = document.querySelector("#allWfsDataTable tbody");

    localDatabase.wfs
        .toArray()
        .then((arr) => {
            arr.forEach((feature) => {
                let newRow = document.createElement("tr");
                newRow.setAttribute("id", `wfsRow-${feature.id}`);
                newRow.classList.add("geodata-row");
                let idElement = document.createElement("td");
                idElement.classList.add("text-center");
                idElement.innerText = feature.id;
                newRow.appendChild(idElement);

                let nameElement = document.createElement("td");
                nameElement.classList.add("text-center");
                nameElement.innerText = feature.name;
                newRow.appendChild(nameElement);

                let groupElement = document.createElement("td");
                groupElement.classList.add("text-center");
                groupElement.innerText = feature.group;
                newRow.appendChild(groupElement);

                let urlElement = document.createElement("td");
                urlElement.classList.add("text-center");
                urlElement.innerText = feature.url;
                newRow.appendChild(urlElement);

                let deleteElement = document.createElement("td");
                deleteElement.classList.add("text-center");
                let deleteButton = document.createElement("button");
                deleteButton.classList.add("bg-red-500");
                deleteButton.classList.add("hover:bg-blue-700");
                deleteButton.classList.add("text-white");
                deleteButton.classList.add("font-bold");
                deleteButton.classList.add("py-2");
                deleteButton.classList.add("px-4");
                deleteButton.classList.add("rounded-full");
                deleteButton.innerText = "Izbriši";
                deleteButton.addEventListener("click", (e) => {
                    _deleteWfs(feature.id)
                });
                deleteElement.appendChild(deleteButton);
                newRow.appendChild(deleteElement);

                allDataTableElement.appendChild(newRow);
            });
        });
}

async function _deleteWfs(id) {
    await localDatabase.to_delete.put({
        id: id,
        type: "wfs"
    });

    let element = document.querySelector(`#wfsRow-${id}`);
    element.parentNode.removeChild(element);

    new Notification("Uspesno izbrisano", {body: "WFS je bil uspesno izbrisani iz podatkovne baze"});

    axios
        .delete(`http://localhost:3000/api/v1/wms/delete/${id}`)
        .then((response) => {
            localDatabase.to_delete
                .where({ id: id, type: "wfs" })
                .delete();
        });
}

module.exports = {loadAllWfs, refreshWfsData}