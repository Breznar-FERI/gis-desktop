const {loadAllFeatures} = require("./feature-loader");
const {bindAddForms, bindAddButtons, bindSyncDBButton} = require("./ui");
const {loadAllWfs} = require("./wfs-loader");
const {bindSearch} = require("./search");
const Mousetrap = require('mousetrap');
const {contextBridge, ipcRenderer} = require("electron")
const {localDatabase} = require("./local-database");
const axios = require("axios");

contextBridge.exposeInMainWorld('darkMode', {
    toggle: () => ipcRenderer.invoke('dark-mode:toggle'),
    system: () => ipcRenderer.invoke('dark-mode:system')
})

window.addEventListener('online', async () => {
    let toDeleteDatabase = await localDatabase.to_delete.toArray();
    toDeleteDatabase.forEach((toDelete) => {
       if (toDelete.type == "wfs") {
           axios.delete(`http://localhost:3000/api/v1/wms/delete/${toDelete.id}`);
       } else if (toDelete.type == "geojson") {
           axios.delete(`http://localhost:3000/api/v1/geojson/delete/${toDelete.id}`);
       }
    });
});

window.addEventListener('DOMContentLoaded', () => {
    Mousetrap.bind('ctrl+f', () => {
        document.querySelector("#searchInput").focus();
    });
    Mousetrap.bind('ctrl+t', () => {
        document.querySelector("#toggleDarkMode").click();
    });

    loadAllFeatures();
    loadAllWfs();
    bindAddForms();
    bindAddButtons();
    bindSearch();
    bindSyncDBButton();
});