const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const FeatureSchema = new Schema({
    type : String,
    layerName : String,
    group : String,
    properties : Object,
    bbox : Array,
    geometry : Object
});

module.exports.Feature = mongoose.model("Feature", FeatureSchema);