const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const FeatureCollectionSchema = new Schema({
    type : String,
    group : String,
    name : String,
    layerName : String,
    bbox : Array,
    features : Array,
});

module.exports.FeatureCollection = mongoose.model("FeatureCollection", FeatureCollectionSchema);