const {saveGeojsonFeatureToDatabase, deleteFeatureFromDatabase, saveGeojsonFeatureCollectionToDatabase,
    deleteFeatureCollectionFromDatabase, loadAllGeojsonFeatureFromDatabase, loadAllGeojsonFeatureCollectionsFromDatabase
} = require("../reposetory/geojson-repository");

const saveGeojson = async (req, res) => {
    let geojson = req.body || {};

    geojson = JSON.parse(Object.keys(geojson)[0] || {});

    if (geojson.type != "FeatureCollection" && geojson.type != "Feature") {
        res.status(500)
            .json({
            "error" : "not geojson format"
        });
        return
    }

    try {
        let layer = null;
        if (geojson.type == "FeatureCollection") {
            layer = await saveGeojsonFeatureCollectionToDatabase(geojson.type, geojson.layerName, geojson.name, geojson.group, geojson.bbox, geojson.features);
        } else if (geojson.type == "Feature") {
            layer = await saveGeojsonFeatureToDatabase(geojson.type, geojson.layerName, geojson.group, geojson.properties, geojson.bbox, geojson.geometry);
        }

        res.json({
            "success" : layer
        })
    } catch (e) {
        res.status(500)
            .json({
                "error" : `There was an error saving features - error: ${e}`
            });
    }
}

const getAllGeojson = async (req, res) => {
    try {
        const allGeojsons = await loadAllGeojsonFeatureFromDatabase() || [];
        allGeojsons.push(...await loadAllGeojsonFeatureCollectionsFromDatabase() || [])


        res.json(allGeojsons);
    } catch (e) {
        res.status(500)
            .json({
            "error" : `There was an error getting all features - error: ${e}`
        });
    }
}

const deleteFeature = async (req, res) => {
    try {
        const deleted = await deleteFeatureFromDatabase(req.params.id || -1);

        res.json(deleted);
    } catch (e) {
        res.status(500)
            .json({
            "error" : `There was an error deleting feature with id: ${req.params.id}, error: ${e}`
        });
    }
}

const deleteFeatureCollection = async (req, res) => {
    try {
        const deleted = await deleteFeatureCollectionFromDatabase(req.params.id || -1);

        res.json(deleted);
    } catch (e) {
        res.status(500)
            .json({
                "error" : `There was an error deleting feature collection with id: ${req.params.id}, error: ${e}`
            });
    }
}


module.exports = { saveGeojson, getAllGeojson, deleteFeature, deleteFeatureCollection }