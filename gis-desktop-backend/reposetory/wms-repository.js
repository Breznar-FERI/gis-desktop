const {WmsLayer} = require("../models/wms-layer");

const saveWmsLayerToDatabase = async (group, layerName, url, properties) => {
    const wmsLayerInstance = new WmsLayer({
        group : group,
        layerName : layerName,
        url : url,
        properties : properties
    });

    const insertWmsLayer = await wmsLayerInstance.save();

    return insertWmsLayer;
}

const loadAllWmsLayersFromDatabase = async () => {
    return WmsLayer.find();
}

const deleteWmsLayerFromDatabase = async (id) => {
    const wmsLayer = await WmsLayer.findOneAndDelete({
        _id : id
    });

    return wmsLayer;
}

module.exports = { saveWmsLayerToDatabase, loadAllWmsLayersFromDatabase, deleteWmsLayerFromDatabase }