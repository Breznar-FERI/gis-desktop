const {Feature} = require("../models/feature");
const {FeatureCollection} = require("../models/feature-collection");

const saveGeojsonFeatureToDatabase = async (type, layerName, group, properties, bbox, geometry) => {
    const featureGeometryInstance = new Feature({
        group : group,
        layerName : layerName,
        type : type,
        bbox : bbox,
        properties : properties,
        geometry : geometry
    });

    const insertFeature = await featureGeometryInstance.save();

    return insertFeature;
}

const saveGeojsonFeatureCollectionToDatabase = async (type, layerName, name, group, bbox, features) => {
    const featurePropertiesInstance = new FeatureCollection({
        group : group,
        layerName : layerName,
        name : name,
        type : type,
        bbox : bbox,
        features : features
    });

    const insertedProperties = await featurePropertiesInstance.save();

    return insertedProperties;
}

const loadAllGeojsonFeatureFromDatabase = async () => {
    return Feature.find();
}

const loadAllGeojsonFeatureCollectionsFromDatabase = async () => {
    return FeatureCollection.find();
}

const deleteFeatureFromDatabase = async (id) => {
    const feature = await Feature.findOneAndDelete({
        _id : id
    });
    return {
        feature : feature
    }
}

const deleteFeatureCollectionFromDatabase = async (id) => {
    const feature = await FeatureCollection.findOneAndDelete({
        _id : id
    });
    return {
        featureCollection : feature
    }
}

module.exports = { saveGeojsonFeatureToDatabase, saveGeojsonFeatureCollectionToDatabase, loadAllGeojsonFeatureFromDatabase, loadAllGeojsonFeatureCollectionsFromDatabase, deleteFeatureFromDatabase, deleteFeatureCollectionFromDatabase }