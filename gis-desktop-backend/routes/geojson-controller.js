let express = require("express");

const {saveGeojson, getAllGeojson, deleteFeature, deleteFeatureCollection} = require("../services/geojson-service");

let router = express.Router();

router.post('/save', saveGeojson);

router.get('/get-all', getAllGeojson);

router.delete('/delete/feature/:id', deleteFeature);

router.delete('/delete/feature-collection/:id', deleteFeatureCollection);

module.exports = router;