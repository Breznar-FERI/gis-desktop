let express = require("express");

let router = express.Router();

router.get('/ping', (req, res) => {
    res.json({
       "status" : "pong"
    });
});

module.exports = router;