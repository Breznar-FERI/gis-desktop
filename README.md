# GIS Desktop

Aplikacija za pregledovanje geoprostorskih podatkov. <br>
Aplikacija sprejme `.geojson` datoteko v formatu *OGC SimpleFeature*. <br>
V aplikacije je tudi mogoce dodati Geoserver `WMS` sloje. <br>
Nalozeni in dodani podatki se shranijo v lokalni shrambi. Uporabnik ima moznost tudi vse podatke shranit v oddaljeno bazo.

<hr>

Glavna aplikacija kjer je pregledovalnik je `gis-desktop-app`, podporna aplikacija za pregled podatkov ki so bili nalozeni v odaljeno bazo je `gis-desktop-database-app`. <br>
API (`gis-desktop-backend`) skrbi za povezavo z odaljeno podatkovno bazo. Na API se povezeta obe namizni aplikaciji in s pomocjo nje shranita ali pridobita podatke iz odaljene baze.
<br>
Primeri Geojsonov za Slovenijo v pravi projekciji se najdejo na naslednji povezavi: `https://github.com/stefanb/gurs-rpe`
<br>
Nekaj primerov datotek bo tudi v mapi `/demo`, tam se tudi nahajajo nastavitve za WMS sloje. 

<hr>

## Gis Desktop App

Glavna aplikacija namenjena pregledovanju geoprostorskih podatkov in podatkov pridobljenih iz geoprostorsskih slojev (Geoserver WMS only...)

![Gis Desktop App](.docs/gis-desktop-app.png)

<hr>

## Gis Desktop Database App

Podporna aplikacija namenjena pregledovanju podatkov ki so bili shranjeni v podatkovno bazo (Izdelana v večini v teku sprotnih vaj...) (!Ne deluje vec zaradi spremenjenih podatkovnih struktur na backendu!)

![Gis Desktop Database App](.docs/gis-desktop-database-app.png)

<hr>