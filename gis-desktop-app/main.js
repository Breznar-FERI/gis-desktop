const { app, BrowserWindow } = require("electron");
const path = require("path");

function createWindow () {
    const win = new BrowserWindow({
        width: 1200,
        height: 800,
        minWidth: 1200,
        minHeight: 800,
        icon: __dirname + "/assets/app.png",
        webPreferences: {
            devTools: true,
            nodeIntegration: true,
            nodeIntegrationInWorker: true,
            preload: path.join(__dirname, "src", "js", "electron", "preload.js")
        }
    });

    win.loadFile(path.join(__dirname, "src", "index.html"));
}

app.setAppUserModelId("Gis Desktop App")

app.whenReady().then(() => {
    createWindow();

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) {
            createWindow();
        }
    })
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
})