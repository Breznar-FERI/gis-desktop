const { _electron: electron, test, expect} = require('@playwright/test');

let window;
let electronApp;

test.beforeAll(async () => {
  electronApp = await electron.launch({ args: ['main.js'], env: { ...process.env, NODE_ENV: 'development' }, });
  window = await electronApp.firstWindow();
});

test("Check window title.", async () => {
  const windowName = await window.title();
  await expect(windowName).toBe("Gis Desktop");
});

test("Check if map has loaded.", async () => {
  // wait so the map loads...
  await new Promise(resolve => setTimeout(resolve, 500));
  await window.screenshot({path: 'playwright-screenshots/geojson-tab-screen.png'});
});

test.afterAll(async () => {
  await window.context().close();
  await window.close();
});
