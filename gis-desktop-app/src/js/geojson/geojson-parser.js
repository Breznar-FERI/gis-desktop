import {
    LAYER_FEATURE_COLLECTION_TYPE, LAYER_FEATURE_TYPE,
    LAYER_FILE_LAYER_GROUP,
} from "../constants/layer-constants.js";
import LayerStorage from "../layers/layer-storage.js";
import MenuController from "../controllers/menu-controller.js";
import LeafletMapController from "../controllers/map-controller.js";
import LeafletGeojsonLayer from "../models/leaflet-geojson-layer.js";
import MapController from "../controllers/map-controller.js";

function parseGeojson(geojson, layername) {
    let featuresLayer = null;

    if (geojson.type === LAYER_FEATURE_COLLECTION_TYPE || geojson.type === LAYER_FEATURE_TYPE) {
        featuresLayer = MapController.addEmptyGeojsonLayer();
    } else {
        return;
    }

    const layerObj = new LeafletGeojsonLayer(layername,
        featuresLayer,
        geojson,
        geojson.type,
        LAYER_FILE_LAYER_GROUP,
        true,
        null);

    LayerStorage.saveLayer(layerObj.layername, layerObj);

    MenuController.addLayerToMenu(layerObj.layername, LAYER_FILE_LAYER_GROUP);

    if (layerObj.visible && featuresLayer !== null) {
        LeafletMapController.addLayerToMap(layerObj.layername);
        MenuController.checkLayerInMenu(layerObj.layername);
    }
}

const GeojsonParser = {
    parseGeojson
}

export default GeojsonParser;