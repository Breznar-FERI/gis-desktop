const MAP_CENTER = {
    lat: 46.056946,
    lng: 14.805751
};

const MAP_START_ZOOM = 8;

export {
    MAP_CENTER,
    MAP_START_ZOOM
}