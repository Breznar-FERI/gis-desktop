import {LAYER_GEOSERVER_TYPE, LAYER_XYZ_TYPE} from "../constants/layer-constants.js";
import LeafletWfsLayer from "../models/leaflet-wfs-layer.js";
import LayerStorage from "./layer-storage.js";
import MenuController from "../controllers/menu-controller.js";
import LeafletMapController from "../controllers/map-controller.js";

function parseLayer(layer, group) {
    let tileLayer = null;

    let url = layer.url || "";
    if (typeof layer.url === "object") {
        url = layer.url.wfs || layer.url.wms || "";
    }

    if (layer.type === LAYER_XYZ_TYPE) {
        tileLayer = L.tileLayer(url, layer.options);
    } else if (layer.type === LAYER_GEOSERVER_TYPE) {
        tileLayer = L.tileLayer.wms(url, layer.options);
    } else {
        return;
    }

    const layerObj = new LeafletWfsLayer(layer.layername,
        tileLayer,
        layer.type,
        group,
        layer.options,
        url,
        layer.url.wfs || null,
        layer.show || false,
        layer.options.cql_filter || null,
        null);

    LayerStorage.saveLayer(layerObj.layername, layerObj);

    MenuController.addLayerToMenu(layerObj.layername, group);

    if (layer.show && tileLayer != null) {
        LeafletMapController.addLayerToMap(layerObj.layername);
        MenuController.checkLayerInMenu(layerObj.layername);
    }
}

function parseGroupOfLayers(layers, group) {
    $.each(layers || [], function (_, layer) {
        parseLayer(layer, group);
    });
}

const LayerParser = {
    parseLayer,
    parseGroupOfLayers
}

export default LayerParser;