import {showMassage} from "../utils/notification.js";
import {
    NOTIFICATION_ERROR,
    NOTIFICATION_SHORT_TIME,
    NOTIFICATION_SUCCESS
} from "../constants/notification-constants.js";
import LayerStorage from "./layer-storage.js";
import LeafletWfsLayer from "../models/leaflet-wfs-layer.js";
import MenuController from "../controllers/menu-controller.js";
import LeafletMapController from "../controllers/map-controller.js";
import LeafletGeojsonLayer from "../models/leaflet-geojson-layer.js";
import MapController from "../controllers/map-controller.js";
import {isObjEmpty} from "../utils/basic-utils.js";

let _isConnected = true;
let _isCheckingStatus = false;

function getAllFeaturesFromDb() {
    $.ajax({
        url: "http://localhost:3000/api/v1/geojson/get-all"
    }).done((response) => {
        response.forEach((layer) => {
            if (!isObjEmpty(LayerStorage.loadLayer(layer.layerName))) {
                return;
            }

            const layerObj = new LeafletGeojsonLayer(layer.layerName,
                MapController.addEmptyGeojsonLayer(),
                layer,
                layer.type,
                layer.group,
                true,
                layer["_id"]);

            LayerStorage.saveLayer(layerObj.layername, layerObj);
            MenuController.addLayerToMenu(layerObj.layername, layerObj.group);
            LeafletMapController.addLayerToMap(layerObj.layername);
            MenuController.checkLayerInMenu(layerObj.layername);
        })
    }).catch(() => {
        new Notification("Napaka", { body: `Ni povezave do podatkovne baze za hranjenje Geojsonov!` });
        if (!_isCheckingStatus) {
            _isConnected = false;
            _isCheckingStatus = true;
            _checkIfServerIsActive();
        }
    });
}

function getAllWmsFromDb() {
    $.ajax({
        url: "http://localhost:3000/api/v1/wms/get-all"
    }).done((response) => {
        response.forEach((layer) => {
            if (!isObjEmpty(LayerStorage.loadLayer(layer.layerName))) {
                return;
            }

            const layerObj = new LeafletWfsLayer(layer.layerName,
                L.tileLayer.wms(layer.url, layer.properties),
                layer.type,
                layer.group,
                layer.properties,
                layer.url,
                null,
                true,
                null,
                layer["_id"]);

            LayerStorage.saveLayer(layerObj.layername, layerObj);
            MenuController.addLayerToMenu(layerObj.layername, layerObj.group);
            LeafletMapController.addLayerToMap(layerObj.layername);
            MenuController.checkLayerInMenu(layerObj.layername);
        });
    }).catch(() => {
        new Notification("Napaka", { body: `Ni povezave do podatkovne baze za hranjenje WFS slojev!` });
        if (!_isCheckingStatus) {
            _isConnected = false;
            _isCheckingStatus = true;
            _checkIfServerIsActive();
        }
    });
}

function saveFeatureToDatabase(layername) {
    let layerData = LayerStorage.loadLayer(layername) || {};

    if (layerData.id != null) {
        deleteFeatureFromDatabase(layername);
        return;
    }

    let layer = layerData.geojson || {};
    layer["layerName"] = layername;
    layer["group"] = layerData.group;

    $.ajax({
        method: "POST",
        url: "http://localhost:3000/api/v1/geojson/save",
        data: JSON.stringify(layer)
    }).done((response) => {
        layerData.id = (response.success || {})["_id"];
        LayerStorage.updateLayer(layername, layerData);
        showMassage(NOTIFICATION_SUCCESS, "Geojson je bil uspesno shranjen v podatkovno bazo.", NOTIFICATION_SHORT_TIME)
    }).catch(() => {
        showMassage(NOTIFICATION_ERROR, "Napaka pri povezovanju na podatkovno bazo.", NOTIFICATION_SHORT_TIME);
        new Notification("Napaka", { body: `Ni povezave do podatkovne baze za hranjenje Geojsonov!` });
        if (!_isCheckingStatus) {
            _isConnected = false;
            _isCheckingStatus = true;
            _checkIfServerIsActive();
        }
    });
}

function saveWmsToDatabase(layername) {
    const layerData = LayerStorage.loadLayer(layername) || {};

    if (layerData.id != null) {
        deleteWmsFromDatabase(layername);
        return;
    }

    const layer = {
        group : layerData.group,
        layerName : layername,
        url : layerData.wms,
        properties : layerData.options
    }

    $.ajax({
        method: "POST",
        url: "http://localhost:3000/api/v1/wms/save",
        data: JSON.stringify(layer)
    }).done((response) => {
        layerData.id = (response.success || {})["_id"];
        LayerStorage.updateLayer(layername, layerData);
        showMassage(NOTIFICATION_SUCCESS, "Wms je bil uspesno shranjen v podatkovno bazo.", NOTIFICATION_SHORT_TIME)
    }).catch(() => {
        showMassage(NOTIFICATION_ERROR, "Napaka pri povezovanju na podatkovno bazo.", NOTIFICATION_SHORT_TIME);
        new Notification("Napaka", { body: `Ni povezave do podatkovne baze za hranjenje WMS slojev!` });
        if (!_isCheckingStatus) {
            _isConnected = false;
            _isCheckingStatus = true;
            _checkIfServerIsActive();
        }
    })
}

function deleteFeatureFromDatabase(layername) {
    let layer = LayerStorage.loadLayer(layername) || {};


    if (layer.id == null) {
        return;
    }

    if (layer.type == "Feature") {
        $.ajax({
            method: "DELETE",
            url: `http://localhost:3000/api/v1/geojson/delete/feature/${layer.id}`
        }).done(() => {
            layer.id = null;
            LayerStorage.updateLayer(layername, layer);
            showMassage(NOTIFICATION_SUCCESS, "Geojson je bil uspesno izbrisan iz podatkovne baze.", NOTIFICATION_SHORT_TIME)
        });
    } else {
        $.ajax({
            method: "DELETE",
            url: `http://localhost:3000/api/v1/geojson/delete/feature-collection/${layer.id}`
        }).done(() => {
            layer.id = null;
            LayerStorage.updateLayer(layername, layer);
            showMassage(NOTIFICATION_SUCCESS, "Geojson je bil uspesno izbrisan iz podatkovne baze.", NOTIFICATION_SHORT_TIME);
        }).catch(() => {
            showMassage(NOTIFICATION_ERROR, "Napaka pri povezovanju na podatkovno bazo.", NOTIFICATION_SHORT_TIME);
            new Notification("Napaka", { body: `Ni povezave do podatkovne baze za hranjenje Geojsonov!` });
            if (!_isCheckingStatus) {
                _isConnected = false;
                _isCheckingStatus = true;
                _checkIfServerIsActive();
            }
        });
    }
}

function deleteWmsFromDatabase(layername) {
    let layer = LayerStorage.loadLayer(layername) || {};

    if (layer.id == null) {
        return;
    }

    $.ajax({
        method: "DELETE",
        url: `http://localhost:3000/api/v1/wms/delete/${layer.id}`
    }).done(() => {
        layer.id = null;
        LayerStorage.updateLayer(layername, layer);
        showMassage(NOTIFICATION_SUCCESS, "Wms je bil uspesno izbrisan iz podatkovne baze.", NOTIFICATION_SHORT_TIME);
    }).catch(() => {
        showMassage(NOTIFICATION_ERROR, "Napaka pri povezovanju na podatkovno bazo.", NOTIFICATION_SHORT_TIME);
        new Notification("Napaka", { body: `Ni povezave do podatkovne baze za hranjenje WMS slojev!` });
        if (!_isCheckingStatus) {
            _isConnected = false;
            _isCheckingStatus = true;
            _checkIfServerIsActive();
        }
    });
}

function connectedToDatabase() {
    return _isConnected;
}

function _checkIfServerIsActive() {
    if (!_isConnected) {
        $(".save-feature-db-btn").addClass("hidden");

        setTimeout(() => {
            $.ajax({
                method: "GET",
                url: `http://localhost:3000/api/v1/ping`
            }).done(() => {
                $(".save-feature-db-btn").removeClass("hidden");

                _isConnected = true;
                _isCheckingStatus = false;

                new Notification("Uspesna povezava", { body: `Aplikacija se je uspesno povezala z podatkovno bazo za shranjevanje podatkov.` });

                getAllFeaturesFromDb();
                getAllWmsFromDb();
            }).catch(() => {
                _checkIfServerIsActive();
            });
        }, 3000);
    }
}

const LayerDatabase = {
    getAllFeaturesFromDb,
    saveFeatureToDatabase,
    deleteFeatureFromDatabase,
    getAllWmsFromDb,
    saveWmsToDatabase,
    deleteWmsFromDatabase,
    connectedToDatabase
}

export default LayerDatabase;