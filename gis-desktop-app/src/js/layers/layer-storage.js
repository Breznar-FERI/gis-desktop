import {
    LAYER_BASE_LAYER_GROUP,
    LAYER_OVERLAY_LAYER_GROUP, LAYER_WMS_LAYER_GROUP
} from "../constants/layer-constants.js";
import MapController from "../controllers/map-controller.js";
import MenuController from "../controllers/menu-controller.js";

let _layerData = {};

let _groupData = {};

let _zIndexBase = 1;

let _zIndexOverlay = 100;

let _zIndexWms = 1000;

function saveLayer(layername, data) {
    if (_groupData[data.group] == null) {
        _groupData[data.group] = L.layerGroup();
    }

    if (data.group === LAYER_BASE_LAYER_GROUP) {
        data.tilelayer.setZIndex(_zIndexBase);
        _zIndexBase++;
    } else if (data.group === LAYER_OVERLAY_LAYER_GROUP) {
        data.tilelayer.setZIndex(_zIndexOverlay);
        _zIndexOverlay++;
    } else if (data.group === LAYER_WMS_LAYER_GROUP) {
        data.tilelayer.setZIndex(_zIndexWms);
        _zIndexWms++;
    }

    _groupData[data.group].addLayer(data.tilelayer);

    _layerData[layername] = data;
}

function updateLayer(layername, data) {
    if (_layerData[layername] == null) {
        return;
    }

    _layerData[layername] = data;
}

function loadLayer(layername) {
    return _layerData[layername] || {};
}

function removeLayer(layername) {
    MenuController.removeLayerFromMenu(layername);
    MapController.removeLayerToMap(layername);
    delete _layerData[layername];
}

function getAllLayer() {
    return _layerData;
}

function getAllLayersAsArray() {
    return Object.values(_layerData);
}

function getVisibleLayers() {
    return getAllLayersAsArray()
        .filter(function (l) {
            return l.visible;
        });
}

const LayerStorage = {
    saveLayer,
    updateLayer,
    loadLayer,
    getAllLayer,
    getVisibleLayers,
    removeLayer
}

export default LayerStorage;