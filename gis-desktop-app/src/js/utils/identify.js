import LeafletMapController from "../controllers/map-controller.js";
import LayerStorage from "../layers/layer-storage.js";
import GConnector from "./geoserver-connector.js";

const _identifyEntryTemplateHeader = $("<div>")
    .addClass("identify-window-body-entry-header flex my-2 px-1")
    .append($("<div>")
        .append($("<i>")
            .addClass("fas-lmap fa-lmap-layer-group")))
    .append($("<span>")
        .addClass("ml-2"))

const _identifyEntryTemplateBody = $("<div>")
    .addClass("identify-window-body-entry grid grid-cols-3 gap-3 pb-2 px-1");

let _identifyAllBtn = null;
let _identifyPopup = null;

function initIdentify() {
    if (LeafletMapController.getMap() == null || (_identifyAllBtn != null && _identifyPopup != null)) {
        return;
    }

    _identifyAllBtn = $("#lMapIdentifyAll");
    _identifyPopup = $("#lMapIdentifyPopup");
    _identifyPopup.find("#lMapIdentifyPopupBody").empty();

    LeafletMapController.getMap().on("click", function (e) {
        if (!_identifyAllBtn.hasClass("active")) {
            return;
        }

        _showIdentifyPopup({
            x: e.originalEvent.clientX,
            y: e.originalEvent.clientY
        });

        $.each(LayerStorage.getVisibleLayers() || [], function (_, layer) {
            let mouseEvent = {
                coordinate: {
                    x: e.latlng.lng,
                    y: e.latlng.lat,
                },
                pixel: e.containerPoint
            }

            let url = layer.getIdentifyUrl({
                mousePoint: mouseEvent
            });

            if (url != null) {
                GConnector
                    .call(url)
                    .then(function (data) {
                        _showData(data, layer);
                    });
            }
        });
    });
}

function _showIdentifyPopup(position) {
    _identifyPopup.css({
        left: position.x,
        top: position.y
    })

    _identifyPopup.removeClass("hidden");
}

function _showData(layerData, layerMetadata) {
    if (layerData.type !== "FeatureCollection") {
        return;
    }

    $.each(layerData.features || {}, function (_, feature) {
        let entry = _identifyEntryTemplateBody.clone();
        let header = _identifyEntryTemplateHeader.clone();

        header
            .find("span")
            .text(layerMetadata.layername);

        _identifyPopup
            .find("#lMapIdentifyPopupBody")
            .append(header);

        $.each(feature.properties || [], function (key, value) {
            entry
                .append($("<span>")
                    .text(key))
                .append($("<span>")
                    .addClass("col-span-2")
                    .text(value || "/"));
        });

        _identifyPopup
            .find("#lMapIdentifyPopupBody")
            .append(entry);
    });
}

export {
    initIdentify
}