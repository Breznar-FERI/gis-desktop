const isObjEmpty = function (obj) {
    return Object.keys(obj).length === 0;
}

export {
    isObjEmpty
}