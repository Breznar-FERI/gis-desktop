//Slovenian mercator
proj4.defs("EPSG:3794", "+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs");
proj4.defs("EPSG:3912", "+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=bessel +towgs84=409.545,72.164,486.872,-3.085957,-5.46911,11.020289,17.919665 +units=m +no_defs +type=crs");
//Laflet default mercator
proj4.defs("EPSG:4326", "+proj=longlat +datum=WGS84 +no_defs +type=crs");

const convertLatLngToSlovenianCoords = function (coord) {
    return proj4("EPSG:4326", "EPSG:3794", coord);
}

const concertSlovenianCoordsToLatLng = function (coord) {
    return proj4("EPSG:3794", "EPSG:4326", coord);
}

const convertCoords = function (from, to, coord) {
    return proj4(from, to, coord);
}

export {
    convertLatLngToSlovenianCoords,
    concertSlovenianCoordsToLatLng,
    convertCoords
}