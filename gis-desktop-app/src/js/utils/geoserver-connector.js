import {showMassage} from "./notification.js";
import {NOTIFICATION_ERROR, NOTIFICATION_MEDIUM_TIME} from "../constants/notification-constants.js";

function call(url) {
    return new Promise(function (resolve, reject) {
        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                resolve(data);
            })
            .catch(function (error) {
                showMassage(NOTIFICATION_ERROR, ("Napak pri pridobivanju sloja iz Geoserverja: ").concat(error), NOTIFICATION_MEDIUM_TIME);
                reject();
            });
    })
}

const GeoserverConnector = {
    call
}

export default GeoserverConnector;