//const $ = require("jquery");

let _notificationContainer = null;

let _notificationTimeout = null;

function initNotification() {
    _notificationContainer = $("#lMapNotification");
}

function showMassage(status, msg, duration) {
    _notificationContainer.addClass(status);
    _notificationContainer.find("#lMapNotificationMsg").text(msg);

    _notificationContainer.removeClass("hidden");

    if (_notificationTimeout != null) {
        clearTimeout(_notificationTimeout);
    }

    _notificationTimeout = setTimeout(function () {
        _notificationContainer.addClass("hidden");
        _notificationContainer.removeClass(status);
        _notificationContainer.find("#lMapNotificationMsg").text("");

        _notificationTimeout = null;
    }, duration);
}

export {
    initNotification,
    showMassage
}