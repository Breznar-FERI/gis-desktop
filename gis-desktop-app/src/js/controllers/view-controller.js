import {NOTIFICATION_INFO, NOTIFICATION_SHORT_TIME} from "../constants/notification-constants.js";
import {initNotification, showMassage} from "../utils/notification.js";
import MenuController from "./menu-controller.js";
import LeafletMapController from "./map-controller.js";
import {initIdentify} from "../utils/identify.js";
import AddFeatureController from "./add-feature-controller.js";
import LayerDatabase from "../layers/layer-database.js";
import FeatureListController from "./feature-list-controller.js";

let _mapComponentTemplate = $("<div id='lMap'>")
    .css({
        position: "absolute"
    });

let _menuComponentTemplate = $("<div id='lMapMenu'>")
    .addClass("map-menu")
    .css({
        position: "absolute"
    })
    .append($("<div id='lMapMenuHeader'>")
        .addClass("map-menu-header map-menu-element flex px-5 py-2")
        .append($("<div>")
            .append($("<i>")
                .addClass("fas-lmap fa-lmap-layer-group")))
        .append($("<span>")
            .addClass("ml-2 flex-auto")
            .text("Sloji"))
        .append($("<div>")
            .addClass("mt-1")
            .append($("<i id='lMapMenuAddFeatureBtn'>")
                .addClass("fas-lmap fa-lmap-plus-circle map-menu-add-feature-btn hover-pointer"))))
    .append($("<div id='lMapMenuShowBtn'>")
        .addClass("map-menu-show-btn hover-pointer")
        .append($("<i>")
            .addClass("fas-lmap fa-lmap-angle-left")))
    .append($("<div id='lMapMenuBody'>")
        .addClass("map-menu-element mt-2")
        .append($("<div>")
            .addClass("map-menu-body-base-header px-2")
            .append($("<span>")
                .text("Osnovni sloji")))
        .append($("<div>")
            .addClass("map-menu-body-base"))
        .append($("<div>")
            .addClass("map-menu-body-overlay-header px-2")
            .append($("<span>")
                .text("Prekrivni sloji")))
        .append($("<div>")
            .addClass("map-menu-body-overlay"))
        .append($("<div>")
            .addClass("map-menu-body-file-header px-2")
            .append($("<span>")
                .text("Datoteke")))
        .append($("<div>")
            .addClass("map-menu-body-file-layers"))
        .append($("<div>")
            .addClass("map-menu-body-wms-header px-2")
            .append($("<span>")
                .text("WMS sloji")))
        .append($("<div>")
            .addClass("map-menu-body-wms-layers")));

let _notificationComponentTemplate = $("<div id='lMapNotification'>")
    .addClass("map-notification-container px-3 py-2 hidden")
    .append($("<h6 id='lMapNotificationMsg'>")
        .addClass("m-0"));

let _identifyAllComponentButtonTemplate = $("<div id='lMapIdentifyAll'>")
    .addClass("identify-all-btn hover-pointer")
    .append($("<i>")
        .addClass("fas-lmap fa-lmap-info"))
    .on("click", function () {
        if ($(this).hasClass("active")) {
            showMassage(NOTIFICATION_INFO, "Izklopljeno poizvedovanje po vseh vidnih slojih", NOTIFICATION_SHORT_TIME);
            $(this).removeClass("active");
            return;
        }
        showMassage(NOTIFICATION_INFO, "Vklopljeno poizvedovanje po vseh vidnih slojih", NOTIFICATION_SHORT_TIME);
        $(this).addClass("active");
    });

let _identifyPopupComponentTemplate = $("<div id='lMapIdentifyPopup'>")
    .addClass("identify-popup-window hidden")
    .append($("<div>")
        .addClass("identify-popup-window-header px-2 py-1")
        .append($("<div>")
            .addClass("flex")
            .append($("<i>")
                .addClass("fas-lmap fa-lmap-info mt-1"))
            .append($("<span>")
                .addClass("ml-1")
                .text("Poizvedba"))
            .append($("<i>")
                .addClass("close-identify-btn fas-lmap fa-lmap-times mt-1")
                .on("click", function () {
                    $("#lMapIdentifyPopup").addClass("hidden");
                }))))
    .append($("<div id='lMapIdentifyPopupBody'>")
        .addClass("identify-popup-window-body"));

let _geojsonFeatureListComponentTemplate = $("<div id='lMapFeatureList'>")
    .addClass("feature-list-window hidden")
    .append($("<div id='lMapFeatureListHeader'>")
        .addClass("feature-list-window-header flex")
        .append($("<span>")
            .addClass("flex-auto"))
        .append($("<div>")
            .addClass("flex-1 hover-pointer")
            .append($("<i>")
                .addClass("fas-lmap fa-lmap-times"))
            .click(() => {
                $("#lMapFeatureList").addClass("hidden");
            })))
    .append($("<div id='lMapFeatureListBody'>")
        .addClass("feature-list-window-body"));

let _addFeatureWindowComponentTemplate = $("<div id='lMapNewFeatureWindow'>")
    .addClass("add-feature-window hidden")
    .append($("<div>")
        .addClass("add-feature-window-form")
        .append($("<div>")
            .addClass("add-feature-window-header flex py-2 px-5")
            .append($("<span>")
                .addClass("flex-auto")
                .text("Dodaj feature"))
            .append($("<div>")
                .addClass("mt-0.5 hover-pointer")
                .append($("<i>")
                    .addClass("fas-lmap fa-lmap-times"))
                .click(() => {
                    AddFeatureController.showAddFeatureWindow(false);
                })))
        .append($("<div>")
            .addClass("add-feature-window-body")
            .append($("<div>")
                .append($("<ul>")
                    .addClass("flex flex-wrap mt-1 text-sm font-medium text-center text-gray-500 border-b border-gray-200")
                    .append($("<li>")
                        .addClass("ml-2")
                        .append($("<a href='#' id='geojsonTabHeader'>")
                            .addClass("add-tab-btn inline-block p-4 rounded-t-lg hover:text-gray-600 hover:bg-gray-50 active")
                            .text("Dodaj Geojson")))
                    .append($("<li>").append($("<a href='#' id='wfsTabHeader'>")
                        .addClass("add-tab-btn inline-block p-4 rounded-t-lg hover:text-gray-600 hover:bg-gray-50")
                        .text("Dodaj WMS nastavitev")))
                )
                .append($("<div id='addGeojsonForm'>")
                    .addClass("mt-2 px-5 new-geojson-form")
                    .append($("<label for='geojsonFileInput'>")
                        .addClass("block mb-2 text-sm font-medium")
                        .text("Izberi geojson:"))
                    .append($("<input id='geojsonFileInput' type='file'>")
                        .addClass("mb-2 pt-1 block w-full text-sm border rounded-lg cursor-pointer focus:outline-none"))
                    .append($("<label for='geojsonLayerName'>")
                        .addClass("block mb-2 text-sm font-medium")
                        .text("Ime sloja:"))
                    .append($("<input id='geojsonLayerName' type='text'>")
                        .addClass("mb-2 block w-full text-sm border rounded-lg cursor-pointer focus:outline-none"))
                    .append($("<button id='addGeojsonButton'>")
                        .addClass("add-geojson-btn bg-white hover:bg-gray-100 text-gray-800 font-semibold py-1 px-4 border border-gray-400 rounded shadow mt-5")
                        .text("Dodaj")
                        .click(AddFeatureController.addGeojsonToMap)))
                .append($("<div id='addWmsForm'>")
                    .addClass("mt-2 px-5 new-wms-form hidden")
                    .append($("<label for='wmsLayerName'>")
                        .addClass("block mb-2 text-sm font-medium")
                        .text("Ime sloja:"))
                    .append($("<input id='wmsLayerName' type='text'>")
                        .addClass("mb-2 block w-full text-sm border rounded-lg cursor-pointer focus:outline-none"))
                    .append($("<label for='wmsLayerUrl'>")
                        .addClass("block mb-2 text-sm font-medium")
                        .text("Url sloja:"))
                    .append($("<input id='wmsLayerUrl' type='text'>")
                        .addClass("mb-2 block w-full text-sm border rounded-lg cursor-pointer focus:outline-none"))
                    .append($("<label for='wmsLayerOptions'>")
                        .addClass("block mb-2 text-sm font-medium")
                        .text("Nastavitve sloja:"))
                    .append($("<textarea id='wmsLayerOptions' rows='7' placeholder='Veljavni JSON format'>")
                        .addClass("block p-2.5 w-full text-sm rounded-lg border"))
                    .append($("<button id='addWmsButton'>")
                        .addClass("add-geojson-btn bg-white hover:bg-gray-100 text-gray-800 font-semibold py-1 px-4 border border-gray-400 rounded shadow mt-5")
                        .text("Dodaj")
                        .click(AddFeatureController.addWmsToMap))
                ))));

let _viewComponent = null;

function _initViewComponents(options) {
    if (_viewComponent != null) {
        return;
    }

    options = options || {};

    _viewComponent = $(("#").concat(options["component_id"] || "lMapComponent"));

    _viewComponent.append(_mapComponentTemplate)
        .append(_menuComponentTemplate)
        .append(_notificationComponentTemplate)
        .append(_identifyAllComponentButtonTemplate)
        .append(_identifyPopupComponentTemplate)
        .append(_addFeatureWindowComponentTemplate)
        .append(_geojsonFeatureListComponentTemplate);

    if (options.menu == null || !options.menu["enabled"]) {
        _viewComponent.find("#lMap")
            .css({
                height: "100%",
                width: "100%"
            });

        _viewComponent.find(".map-menu-element")
            .addClass("hidden");

        _viewComponent.find("#lMapMenuShowBtn")
            .remove();

        return;
    }

    let mapWidth = 100 - options.menu["width_percentage"] || 0;

    _viewComponent.find("#lMap").css({
        height: "100%",
        width: mapWidth.toString().concat("%"),
        left: (100 - mapWidth).toString().concat("%")
    });

    _viewComponent.find("#lMapMenu").css({
        height: "100%",
        width: (100 - mapWidth).toString().concat("%"),
        left: "0%"
    });

    if (!options.menu["show"] || mapWidth === 100) {
        _viewComponent.find("#lMap")
            .css({
                width: "100%",
                left: "0%"
            });

        _viewComponent.find("#lMapMenu")
            .addClass("map-menu-hide");

        _viewComponent.find("#lMapMenuShowBtn i")
            .removeClass("fa-lmap-angle-left")
            .addClass("fa-lmap-angle-right")

        _viewComponent.find(".map-menu-element")
            .addClass("hidden");
    }
}

function openMap(options) {
    _initViewComponents(options);
    initNotification();

    AddFeatureController.initAddFeature(options);
    MenuController.initMenu(options);
    LeafletMapController.initMap(options);
    FeatureListController.initFeatureList(options);

    initIdentify();

    LayerDatabase.getAllFeaturesFromDb();
    LayerDatabase.getAllWmsFromDb();
}

function getViewComponent() {
    return _viewComponent;
}

const ViewController = {
    openMap,
    getViewComponent
}

export default ViewController;