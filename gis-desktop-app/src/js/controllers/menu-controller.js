import App from "../app.js";
import LeafletMapController from "./map-controller.js";
import {
    LAYER_BASE_LAYER_GROUP,
    LAYER_FILE_LAYER_GROUP,
    LAYER_OVERLAY_LAYER_GROUP, LAYER_WMS_LAYER_GROUP
} from "../constants/layer-constants.js";
import AddFeatureController from "./add-feature-controller.js";
import LayerStorage from "../layers/layer-storage.js";
import LayerDatabase from "../layers/layer-database.js";
import FeatureListController from "./feature-list-controller.js";

let _menuItemTemplate = $("<div>")
    .addClass("map-menu-layer map-menu-element flex px-5 my-1")
    .append($("<div>")
        .addClass("show-layer-btn hover-pointer")
        .append($("<i>")
            .addClass("far-lmap fa-lmap-square")))
    .append($("<span>")
        .addClass("map-menu-layer-name flex-auto ml-2"))

let _menu = null;

function initMenu(options) {
    options = options || {};
    if (_menu != null) {
        return;
    }

    _menu = $("#lMapMenu");

    if (options.menu == null || !options.menu["enabled"]) {
        App.Globals["MenuWidth"] = 0;
        return;
    }

    App.Globals["MenuWidth"] = options.menu["width_percentage"];

    _menu.find("#lMapMenuShowBtn").on("click", _hideMenu);
    _menu.find("#lMapMenuAddFeatureBtn").on("click", () => {
        AddFeatureController.showAddFeatureWindow(true)
    });
}

function addLayerToMenu(layername, group) {
    let item = _menuItemTemplate.clone();

    item.attr("data-layername", layername)
    item.find(".map-menu-layer-name").text(layername);

    item.find(".show-layer-btn").on("click", function () {
        let layername = $(this).parent().data("layername");
        LeafletMapController.toggleLayerVisible(layername);

        if ($(this).children().hasClass("fa-lmap-square")) {
            $(this).children()
                .removeClass("fa-lmap-square");
            $(this).children()
                .addClass("fa-lmap-check-square");
            return;
        }

        $(this).children()
            .removeClass("fa-lmap-check-square");
        $(this).children()
            .addClass("fa-lmap-square");
    });

    if (group === LAYER_BASE_LAYER_GROUP) {
        _menu.find(".map-menu-body-base").append(item);
    } else if (group === LAYER_OVERLAY_LAYER_GROUP) {
        _menu.find(".map-menu-body-overlay").append(item);
    } else if (group === LAYER_FILE_LAYER_GROUP) {
        item.append($("<div>")
                .addClass("open-feature-list-btn hover-pointer")
                .append($("<i>")
                    .addClass("fas-lmap fa-lmap-list"))
                .click(() => {
                    FeatureListController.openFeatureList(layername);
                }))
            .append($("<div>")
                .addClass("save-feature-db-btn ml-2 hover-pointer")
                .append($("<i>")
                    .addClass("fas-lmap fa-lmap-save"))
                .click(() => {
                    LayerDatabase.saveFeatureToDatabase(layername);
                }))
            .append($("<div>")
                .addClass("delete-file-btn ml-2 hover-pointer")
                .append($("<i>")
                    .addClass("far-lmap fa-lmap-trash-alt"))
                .click(() => {
                    LayerDatabase.deleteFeatureFromDatabase(layername);
                    LayerStorage.removeLayer(layername);
                }));

        if (!LayerDatabase.connectedToDatabase()) {
            item.find(".save-feature-db-btn").addClass("hidden");
        }

        _menu.find(".map-menu-body-file-layers").append(item);
    } else if (group === LAYER_WMS_LAYER_GROUP) {
        item.append($("<div>")
                .addClass("save-feature-db-btn ml-2 hover-pointer")
                .append($("<i>")
                    .addClass("fas-lmap fa-lmap-save"))
                .click(() => {
                    LayerDatabase.saveWmsToDatabase(layername);
                }))
            .append($("<div>")
                .addClass("delete-file-btn ml-2 hover-pointer")
                .append($("<i>")
                    .addClass("far-lmap fa-lmap-trash-alt"))
                .click(() => {
                    LayerDatabase.deleteWmsFromDatabase(layername);
                    LayerStorage.removeLayer(layername);
                }));
        _menu.find(".map-menu-body-wms-layers").append(item);
    }
}

function toggleLayerInMenu(layername) {
    let item = $((".map-menu-layer[data-layername='").concat(layername).concat("']"));

    LeafletMapController.toggleLayerVisible(layername);

    if (item.find(".show-layer-btn .far-lmap").hasClass("fa-lmap-square")) {
        item.find(".show-layer-btn .far-lmap")
            .removeClass("fa-lmap-square");
        item.find(".show-layer-btn .far-lmap")
            .addClass("fa-lmap-check-square");
        return;
    }

    item.find(".show-layer-btn .far-lmap")
        .removeClass("fa-lmap-check-square");
    item.find(".show-layer-btn .far-lmap")
        .addClass("fa-lmap-square");
}

function checkLayerInMenu(layername) {
    let item = $(".map-menu-layer[data-layername='" + layername + "']");

    item.find(".show-layer-btn .far-lmap")
        .removeClass("fa-lmap-square");
    item.find(".show-layer-btn .far-lmap")
        .addClass("fa-lmap-check-square");
}

function removeLayerFromMenu(layername) {
    $(".map-menu-layer[data-layername='" + layername + "']").remove();
}

function _hideMenu() {
    if (_menu.hasClass("map-menu-hide")) {
        let menuWidth = LeafletMapController.setMapHolderWidth(100 - App.Globals.MenuWidth);

        _menu.css({
            width: menuWidth
        });

        _menu.removeClass("map-menu-hide");
        _menu.find(".map-menu-element")
            .removeClass("hidden");

        _menu.find("#lMapMenuShowBtn")
            .children()
            .removeClass("fa-lmap-angle-right");
        _menu.find("#lMapMenuShowBtn")
            .children()
            .addClass("fa-lmap-angle-left");

        return;
    }

    _menu.addClass("map-menu-hide");
    _menu.find(".map-menu-element")
        .addClass("hidden");
    _menu.find("#lMapMenuShowBtn")
        .children()
        .removeClass("fa-lmap-angle-left");
    _menu.find("#lMapMenuShowBtn")
        .children()
        .addClass("fa-lmap-angle-right");

    LeafletMapController.setMapHolderWidth(100);
}

const MenuController = {
    initMenu,
    addLayerToMenu,
    toggleLayerInMenu,
    checkLayerInMenu,
    removeLayerFromMenu
}

export default MenuController;