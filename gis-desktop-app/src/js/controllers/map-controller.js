import App from "../app.js";
import {MAP_CENTER, MAP_START_ZOOM} from "../constants/map-constants.js";
import LayerParser from "../layers/layer-parser.js";
import {
    LAYER_BASE_LAYER_GROUP, LAYER_FEATURE_COLLECTION_TYPE, LAYER_FEATURE_TYPE,
    LAYER_GEOSERVER_TYPE,
    LAYER_OVERLAY_LAYER_GROUP,
    LAYER_XYZ_TYPE
} from "../constants/layer-constants.js";
import LayerStorage from "../layers/layer-storage.js";
import {isObjEmpty} from "../utils/basic-utils.js";


let mapInitSettings = null;

var _mapHolder = null;

var _map = null;

var latlng = null;

function initMap(options) {
    mapInitSettings = options || {};

    if (_map != null || mapInitSettings.map == null) {
        return
    }

    App.Globals["MapWidth"] = mapInitSettings.map["width_percentage"] || 100 - App.Globals.MenuWidth;

    latlng = L.latLng(options.map.center.latitude || MAP_CENTER.lat, options.map.center.longitude || MAP_CENTER.lng);

    _mapHolder = $("#lMap");

    _map = L.map("lMap", {
        center: latlng,
        zoom: options.map.center.zoom || MAP_START_ZOOM,
        zoomControl: options.map.zoom
    });

    if (!options.map.zoom) {
        _map.touchZoom.disable();
        _map.doubleClickZoom.disable();
        _map.scrollWheelZoom.disable();
        _map.boxZoom.disable();
        _map.keyboard.disable();
    }

    L.control.scale({
        metric: true,
        imperial: false
    }).addTo(_map);

    LayerParser.parseGroupOfLayers(mapInitSettings.layers.base, LAYER_BASE_LAYER_GROUP);

    LayerParser.parseGroupOfLayers(mapInitSettings.layers.overlay, LAYER_OVERLAY_LAYER_GROUP);

    _map.invalidateSize(true);
}

function setMapHolderWidth(widthInProc) {
    if (widthInProc == null || widthInProc < 0 || widthInProc > 100) {
        return "";
    }

    let menuWidth = 100 - widthInProc;

    _mapHolder.css({
        width: widthInProc.toString().concat("%"),
        left: menuWidth.toString().concat("%")
    });

    _map.invalidateSize(true);

    return menuWidth.toString().concat("%");
}

function addLayerToMap(layername) {
    let layer = LayerStorage.loadLayer(layername);

    if (isObjEmpty(layer)) {
        return;
    }

    if (layer.type === LAYER_GEOSERVER_TYPE || layer.type === LAYER_XYZ_TYPE) {
        layer.tilelayer.addTo(_map);
    } else if (layer.type === LAYER_FEATURE_TYPE || layer.type === LAYER_FEATURE_COLLECTION_TYPE) {
        layer.tilelayer.addData(layer.geojson);
    }
}

function addEmptyGeojsonLayer() {
    return L.geoJSON().addTo(_map);
}

function removeLayerToMap(layername) {
    let layer = LayerStorage.loadLayer(layername);

    if (isObjEmpty(layer)) {
        return;
    }

    layer.tilelayer.removeFrom(_map);
}

function toggleLayerVisible(layername) {
    let layer = LayerStorage.loadLayer(layername);

    if (isObjEmpty(layer)) {
        return;
    }

    if (layer.visible) {
        layer.tilelayer.removeFrom(_map);
    } else {
        layer.tilelayer.addTo(_map);
    }

    layer.visible = !layer.visible;
}

function getMap() {
    return _map;
}

function getMapHolder() {
    return _mapHolder;
}

const MapController = {
    initMap,
    setMapHolderWidth,
    addLayerToMap,
    removeLayerToMap,
    toggleLayerVisible,
    getMap,
    getMapHolder,
    addEmptyGeojsonLayer
}

export default MapController;