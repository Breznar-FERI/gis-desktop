import LayerStorage from "../layers/layer-storage.js";
import {LAYER_FEATURE_COLLECTION_TYPE} from "../constants/layer-constants.js";

let _featureListWindow = null;

function initFeatureList(options) {
    options = options || {};

    if (_featureListWindow != null) {
        return;
    }

    _featureListWindow = $("#lMapFeatureList");
}

async function openFeatureList(layername) {
    _featureListWindow.removeClass("hidden");
    _featureListWindow.find("#lMapFeatureListHeader span").text(layername);

    _featureListWindow.find("#lMapFeatureListBody").empty();

    const layer = LayerStorage.loadLayer(layername);

    if (layer.type === LAYER_FEATURE_COLLECTION_TYPE) {
        layer.geojson.features.forEach((feature) => {
           const featureComponent = $("<div>")
               .addClass("feature-component");

           $.each(feature.properties || [], function (key, value) {
               featureComponent
                   .append($("<div>")
                       .addClass("flex")
                       .append($("<span>")
                           .text(key)
                           .addClass("flex-auto feature-component-key"))
                       .append($("<span>")
                           .text(value || "/")
                           .addClass("flex-auto")));
           });

           _featureListWindow.find("#lMapFeatureListBody").append(featureComponent);
        });
    } else {
        const featureComponent = $("<div>")
            .addClass("feature-component");

        $.each(layer.geojson.properties || [], function (key, value) {
            featureComponent
                .append($("<div>")
                    .addClass("flex")
                    .append($("<span>")
                        .text(key)
                        .addClass("flex-auto feature-component-key"))
                    .append($("<span>")
                        .text(value || "/")
                        .addClass("flex-auto")));
        });

        _featureListWindow.find("#lMapFeatureListBody").append(featureComponent);
    }
}

const FeatureListController = {
    initFeatureList,
    openFeatureList
}

export default FeatureListController;