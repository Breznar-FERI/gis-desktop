import {isObjEmpty} from "../utils/basic-utils.js";
import GeojsonParser from "../geojson/geojson-parser.js";
import LayerParser from "../layers/layer-parser.js";
import {LAYER_WMS_LAYER_GROUP} from "../constants/layer-constants.js";

let _addFeatureWindow = null

let _tmpAddedJsonFile = null;

function initAddFeature(options) {
    options = options || {};

    if (_addFeatureWindow != null) {
        return;
    }

    _addFeatureWindow = $("#lMapNewFeatureWindow");

    _addFeatureWindow.find("#geojsonTabHeader").on("click", () => {
        _addFeatureWindow.find("#geojsonTabHeader").addClass("active");
        _addFeatureWindow.find("#wfsTabHeader").removeClass("active");
        _addFeatureWindow.find("#addGeojsonForm").removeClass("hidden");
        _addFeatureWindow.find("#addWmsForm").addClass("hidden");
    });

    _addFeatureWindow.find("#wfsTabHeader").on("click", () => {
        _addFeatureWindow.find("#geojsonTabHeader").removeClass("active");
        _addFeatureWindow.find("#wfsTabHeader").addClass("active");
        _addFeatureWindow.find("#addGeojsonForm").addClass("hidden");
        _addFeatureWindow.find("#addWmsForm").removeClass("hidden");
    });

    _addFeatureWindow.find("#geojsonFileInput").on("change", _onChangeInputFile);
}

function showAddFeatureWindow(show) {
    if (show) {
        _addFeatureWindow.removeClass("hidden");
        return;
    }

    _addFeatureWindow.addClass("hidden");
}

function addGeojsonToMap() {
    const layerName = _addFeatureWindow.find("#geojsonLayerName").val() || null;

    if (layerName == null || _tmpAddedJsonFile == null || isObjEmpty(_tmpAddedJsonFile)) {
        return;
    }

    showAddFeatureWindow(false);
    GeojsonParser.parseGeojson(_tmpAddedJsonFile, layerName);
}

function addWmsToMap() {
    const layerName = _addFeatureWindow.find("#wmsLayerName").val() || null;
    const wmsUrl = _addFeatureWindow.find("#wmsLayerUrl").val() || null;
    const wmsOptions = JSON.parse(_addFeatureWindow.find("#wmsLayerOptions").val() || "{}");

    if (layerName == null || wmsUrl == null || isObjEmpty(wmsOptions)) {
        return;
    }

    showAddFeatureWindow(false);
    LayerParser.parseLayer({
        "layername" : layerName,
        "type" : "geoserver",
        "url" : {
            "wms" : wmsUrl
        },
        "options" : wmsOptions,
        "show" : true
    }, LAYER_WMS_LAYER_GROUP);
}

function _onChangeInputFile(event) {
    var reader = new FileReader();
    reader.onload = _onFileInputReaderLoad;
    reader.readAsText(event.target.files[0]);
}

function _onFileInputReaderLoad(event){
    _tmpAddedJsonFile = JSON.parse(event.target.result);
}

const AddFeatureController = {
    initAddFeature,
    showAddFeatureWindow,
    addGeojsonToMap,
    addWmsToMap
}

export default AddFeatureController;