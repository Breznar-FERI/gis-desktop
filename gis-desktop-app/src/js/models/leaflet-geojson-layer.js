import {LAYER_FEATURE_COLLECTION_TYPE} from "../constants/layer-constants.js";

class LeafletGeojsonLayer {

    type = null;
    group  = null;
    layername = null;
    tilelayer = null;
    geojson = null;
    visible = null;
    id = null;

    constructor(layername, tilelayer, geojson, type, group, visible, id) {
        this.layername = layername;
        this.tilelayer = tilelayer;
        this.geojson = geojson;
        this.type = type;
        this.group = group;
        this.visible = visible;
        this.id = id;
    }

    getAllFeatures() {
        if (this.type == LAYER_FEATURE_COLLECTION_TYPE) {
            return this.geojson.features || [];
        } else {
            return [
                this.geojson || {}
            ];
        }
    }

}

export default LeafletGeojsonLayer;