import {isObjEmpty} from "../utils/basic-utils.js";
import {LAYER_GEOSERVER_TYPE, LAYER_WFS_GEO_CQL_FILTER} from "../constants/layer-constants.js";
import LeafletMapController from "../controllers/map-controller.js";

class LeafletWfsLayer {

    type = null;
    group  = null;
    wms = null;
    wfs = null;
    layername = null;
    tilelayer = null;
    options = null;
    visible = null;
    cql = null;
    id = null;

    constructor(layername, tilelayer, type, group, options, wms, wfs, visible, cql, id) {
        this.layername = layername;
        this.tilelayer = tilelayer;
        this.type = type;
        this.group = group;
        this.options = options;
        this.wms = wms;
        this.wfs = wfs;
        this.visible = visible;
        this.cql = cql;
        this.id = id;
    }

    getIdentifyUrl(options) {
        if (isObjEmpty(options)) {
            return null;
        }

        let params = { };
        let url = (this.wfs || this.wms);
        let cql = options.cql || null;

        if (!url.includes("?")) {
            url = url.concat("?");
        }

        if (this.type === LAYER_GEOSERVER_TYPE) {
            params = {
                SERVICE: 'WMS',
                VERSION: '1.1.1',
                REQUEST: 'GetFeatureInfo',
                LAYERS: this.options.layers,
                QUERY_LAYERS: this.options.layers,
                STYLES: this.options.styles,
                BBOX: LeafletMapController.getMap().getBounds().toBBoxString(),
                WIDTH: LeafletMapController.getMap().getSize().x,
                HEIGHT: LeafletMapController.getMap().getSize().y,
                SRS: "EPSG:4258",
                INFO_FORMAT: 'application/json',
                FORMAT: 'image/png',
                x: parseInt(options.mousePoint.pixel.x), //both values need to be without decimal places
                y: parseInt(options.mousePoint.pixel.y)
            }

            if ((cql || this.cql) != null) {
                params["CQL_FILTER"] = cql || this.cql;
            }
        } else {
            return null;
        }

        return url.concat($.param((params)));
    }

}

export default LeafletWfsLayer;